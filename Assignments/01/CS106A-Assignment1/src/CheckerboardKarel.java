/*
 * TODO: comment this program
 */

import stanford.karel.*;

public class CheckerboardKarel extends SuperKarel {
	

	public void run ()
	{
		fillOneColumn();
		while(frontIsClear())
		{	
			move();
			if(leftIsClear())
			{
				turnLeft();
				move();
				turnRight();
				fillOneColumn();
			}

			if(frontIsClear())
			{
				move();
				fillOneColumn();
			}			
		}		
	}
	
	private void fillOneColumn()
	{
		turnLeft();
		putBeeper();
		while(frontIsClear())
		{
			move();
			if(frontIsClear())
			{
				move();
				putBeeper();
			}			
		}
		turnAround();
		while(frontIsClear())
		{
			move();
		}
		turnLeft();
	}	
	
}
