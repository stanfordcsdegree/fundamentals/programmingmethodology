/*
 * TODO: comment this program
 */

import stanford.karel.*;

public class CollectNewspaperKarel extends SuperKarel {
	
	// TODO: write the code to implement this program
	public void run ()
	{
		moveToNewspaper();
		pickBeeper();
		turnAround();
		returnToPosition();		
	}
	
	private void moveToNewspaper ()
	{
		turnRight();
		move();
		turnLeft();
		for(int i = 0; i < 3; i++)
		{
			move();
		}
	}
	
	private void returnToPosition ()
	{
		for(int i = 0; i < 3; i++)
		{
			move();
		}
		turnRight();
		move();
		turnRight();
	}	
}
