/*
 * TODO: comment this program
 */

import stanford.karel.*;

public class StoneMasonKarel extends SuperKarel {
	
	// TODO: write the code to implement this program
	public void run ()
	{		
		repairOneArch();
		while(frontIsClear())
		{
			moveToNextArch();
			repairOneArch();
		}
	}
	private void repairOneArch()
	{
		turnLeft();
		while(frontIsClear())
		{
			if(noBeepersPresent())
			{
				putBeeper();
			}
			move();
		}
		if(noBeepersPresent())
		{
			putBeeper();
		}		
		turnAround();
		while(frontIsClear())
		{
			move();
		}	
		turnLeft();
	}
	
	private void moveToNextArch()
	{
		for (int i = 0; i < 4 && frontIsClear(); i++)
		{
			move();
		}
	}

}
